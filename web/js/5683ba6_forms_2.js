function postForm($form, callback) {

    /*
     * Get all form values
     */
    var values = {};
    var x = $form.serializeArray();
    $.each($form.serializeArray(), function (i, field) {
        values[field.name] = field.value;
    });

    /*
     * Throw the form values to the server!
     */
    $.ajax({
        type: $form.attr('method'),
        url: $form.attr('action'),
        data: values,
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            callback($form, data);
        }
    });

}

$(document).ready(function () {

    $('form').submit(function (e) {
        e.preventDefault();

        postForm($(this), function ($form, response) {
            $form.append('bla');
        });

        return false;
    });

});