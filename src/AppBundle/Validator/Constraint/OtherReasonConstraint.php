<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class OtherReasonConstraint extends Constraint
{
    public $message = 'Please fill in the reason';
}