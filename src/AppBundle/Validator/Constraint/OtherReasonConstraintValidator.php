<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class OtherReasonConstraintValidator extends ConstraintValidator
{
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }

    public function validate($protocol, Constraint $constraint)
    {
        if ($protocol->getReason() == 3 && empty($protocol->getReasonText())) {
            $this->context->buildViolation($constraint->message)
                ->atPath('reason')
                ->addViolation();
        }
    }
}