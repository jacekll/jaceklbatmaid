<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Validator\Constraint as JacekAssert;

/**
 * @JacekAssert\OtherReasonConstraint
 */
class Refusal
{
    /**
     * @Assert\NotBlank
     *
     * @var int
     */
    private $reason;

    /**
     * @var string
     */
    private $reasonText;

    /**
     * @return int
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * @param int $reason
     */
    public function setReason($reason)
    {
        $this->reason = $reason;
    }

    /**
     * @return string
     */
    public function getReasonText()
    {
        return $this->reasonText;
    }

    /**
     * @param string $reasonText
     */
    public function setReasonText($reasonText)
    {
        $this->reasonText = $reasonText;
    }
}