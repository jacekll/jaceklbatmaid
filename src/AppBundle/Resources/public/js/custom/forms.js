function postForm($form, callback) {

    var values = {};

    $.ajax({
        type: $form.attr('method'),
        url: $form.attr('action'),
        data: $form.serialize(),
        dataType: 'json',
        success: function (data) {
            callback($form, data);
        }
    });

}

$(document).ready(function () {

    $('form').submit(function (e) {
        e.preventDefault();

        postForm($(this), function ($form, response) {
            $form.append('bla');
        });

        return false;
    });

});