<?php

namespace AppBundle\Controller;

use AppBundle\Form\DeclineType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $form = $this->createForm(DeclineType::class, null, ['action' => $this->generateUrl('decline_process')]);

        return [
            'declineform' => $form->createView()
        ];
    }

    /**
     * @Route("/decline", name="decline_process")
     */
    public function declineAction(Request $request)
    {

        $form = $this->createForm(DeclineType::class);
        $res = $form->handleRequest($request);

        if (!$form->isValid()) {
            return new JsonResponse($form->getErrors()->getChildren(), 400);
        }

        return new JsonResponse(['location' => $this->generateUrl('decline_done')]);
    }

    /**
     * @Template()
     * @Route("/done", name="decline_done")
     */
    public function doneAction()
    {

    }
}
