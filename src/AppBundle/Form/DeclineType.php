<?php

namespace AppBundle\Form;

use AppBundle\Entity\Refusal;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DeclineType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('reason', ChoiceType::class,
            [
                'expanded' => true,
                'multiple' => false,
                'choices' => [
                    'I have a doctors appointment' => 1,
                    'The travel is too long' => 2,
                    'Other' => 3
            ]
        ]
        );
        $builder->add('reasonText', TextareaType::class, ['label' => false])->setRequired(false);
        $builder->add('submit', SubmitType::class, ['label' => 'Decline']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Refusal::class,
        ]);
    }

    public function getName()
    {
        return 'DeclineType';
    }
}